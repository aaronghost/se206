# Compte-rendu du TP sur UPPAAL : mise en oeuvre et vérification de l'algorithme de Fischer

Compte-rendu de __Bastien Lecoeur__

# Mise en oeuvre de l'algorithme de Fischer

On cherche ici à mettre en oeuvre l'algorithme de Fischer afin de s'assurer qu'un seul processus peut accéder à la section critique commune aux processus envisagés.

1. Afin de mettre en oeuvre l'algorithme de Fischer, on complète le modèle fourni.
   Conformément à ce qui est demandé dans le code, on a mis une contrainte de c sur l'ensemble des états à l'exclusion ```L4Lotk``` et ```L0``` qui peuvent être aussi longs que désirés (les transitions zeno sur ces états n'empêchent pas la vérification de la propriété)

2. L'algorithme de Fischer n'est pas vérifié pour les transitions suivantes du système :
  ```
  int has_cs=0;
  const int c =4;
  const int CS =30;
  const int delay = 3;
  ```
  On peut examiner une trace de contre-exemple afin de vérifier où se situe le problème :
  ![fischer2.png](fischer2.png)

  On constate alors que le Processus1 quitte la phase d'attente (waiting) avant que le Processus2 n'ai fini l1 (dont la transition sortante affecte has_cs à l'ID du processus).

3. On corrige donc le défaut de paramétrage de la question précédente.
   On peut déterminer la valeur minimum à affecter à waiting pour vérifier la propriété par dichotomie. On trouve que la valeur minimal à attendre est __c+1__ ce qui correspond au cas ou un processus effectuerait l'étape c1 au moment ou l'autre entrerait en attente.

La paramètre de waiting a une valeur qui est déterminée par des variables du modèles (temps d'attente sur un état et nombre d'états du modèles). Ces paramètres sont indépendants du nombre de processus et l'on peut donc procéder à une implémentation de Fischer en connaissant les caractéristiques sur l'attente d'un processus (ce qui se traduit en temps d'exécution d'instructions élémentaires dans une application concrète).

# Problème d'équité avec l'algorithme de Fischer

On cherche maintenant à observer des phénomènes de famines entre les processus utilisant l'algorithme de Fischer et voir s'il on peut garantir un accès à une ressource critique à un moment donné de l'exécution pour tous les programmes.

1. Il est impératif de borner le temps passé en section critique pour pouvoir observer des phénomènes de famine "évitables". En effet, si le temps en section critique n'est pas borné, un processus peut entrer en section critique et ne plus jamais en sortir. On observe alors que le processus occupe en permanence la section critique, ce qui est normal puisque rien ne l'en empêche. On doit donc borner le temps passé en section critique afin de forcer le processus à en sortir et laisser une chance aux autres processus d'y rentrer.

2. Lorsque l'on borne le temps en section critique, on peut constater :
    * Des contre-exemples zeno qui se composent d'un nombre infini de transitions à l'état L0 et avec des cycles L0/ L0goto sont effectuées en cycle et le processus qui les effectue ne parvient donc jamais à la zone critique.
    On peut retirer ce comportement zeno pour la suite du TP en bornant le temps passé en L0  et dans le cycle (L0 / L0goto).

    ![fischer3-zeno.png](fischer3-zeno.png)

    * Un contre-exemple qui est celui attendu, en effet, il existe une infinité de configurations dans laquelle le processus ne quitte pas la boucle L0/L0goto car il n'est pas en L0 quand la section critique est libre.

3. On ajoute un état supplémentaire après le passage dans la section critique qui permet de passer en attente active le temps que l'autre processus atteigne la section critique. Le temps a donner à cette attente est compliqué à estimer car il dépend des cycles que l'autre processus peut faire entre tous les états utilsant l0, l0goto, l1, l2, waiting, l3 et l3goto.

    On constate que pour seulement deux processus, il faut au minimum 21 instants en attente pour garantir l'absence de famine, pour trois processus, il en faut 64.

    La solution temporelle est donc inappropriée, elle conduit à un temps réel d'utilisation faible de la section critique et la contrainte temporelle sur l'état de retour dépend fortement du nombre de processus à exécuter (inconnu et variable dans un environnement réel).

4. On met en oeuvre une solution à partir d'un tableau contenant le nombre de fois par lequel chaque processus est passé. Chaque processus vérifie combien de fois, il a accédé à l'état critique et se met alors de lui-même en retrait pour laisser passer l'autre processus.

Conclusion : La dernière méthode enst plus efficace, elle permet d'obtenir un algorithme de Fischer équitable et d'implémenter les fonctionnalités attendues d'un algorithme de synchronisation.

__NB :__ Plusieurs fichiers sont joints avec ce rapport.
* __fischer-tpt.xml__ présente le modèle de la partie II
* __fischer-tpt-3-base.xml__ présente le modèle utilisé dans la partie III et le contre-exemple que l'on peut obtenir hors zeno
* __fischer-tpt3-etape-attente.xml__ présente le modèle avec un état pour l'attente active (question 3)
* __fischer-tpt3-variable.xml__ présente le modèle de de la dernière question.
