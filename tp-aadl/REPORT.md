# Rapport sur le TP AADL

Rapport de Bastien Lecoeur

# Description de l'architecture

On complète d'abord les deux thread manquants dans le code du software et qui permettent de générer le signal cmd à partir de la position.

```AADL
thread speed_computation extends periodic_thread
		features
			pos: in data port data_pkg::position;
			speed: out data port data_pkg::speed;
		flows
			fl_speed: flow path pos -> speed;
		properties
			Period => 50 ms;
			Deadline => 50 ms;		
	end speed_computation;

	thread command_computation extends periodic_thread
		features
			speed: in data port data_pkg::speed;
			acceleration: out data port data_pkg::acceleration;
		flows
			fl_acceleration: flow path speed -> acceleration;
		properties
			Period => 200 ms;
			Deadline => 200ms;
	end command_computation;
```

On instancie ensuite ces deux threads dans l'implémentation du software.
On a pas besoin à ce moment de spécifier une quelconque propriété puisque celles-ci sont gérées dans les thread.

On peut visualiser le diagramme correspondant au software :
![softwareimpl](sofwareimpl.jpg)

On ajoute ensuite dans le code de l'implémentation les différents temps d'exécution rta.

On peut ensuite valider le modèle avec AADL Inspector.

# Vérifications des contraintes temporelles

A l'aide d'AADLInspector, on peut observer que la tâche est planifiable car elle occupe moins de 100% du temps le processeur.

# Analyse du temps de latence

On cherche tout d'abord à optimiser le temps de latence donc on spécifie dans l'implémentation que l'on veut que les valeurs soient disponibles le plus rapidement possible avec la propriété Timing immediate que l'on applique à toutes les connexions.

On regarde ensuite la valeur du pire temps de réponse dans AADLInspector. On obtient :
|Signal|Temps minimum|
|:-----:|:----------:|
|Obst_detection| 25ms|
|speed_comput|15ms|
|command_comput|95ms|

Avec ces nouvelles caractéristiques, les contraintes de latence ne sont toujours pas vérifiées.
On met à jour le modèle et on examine la réponse pire des cas à nouveau dans AADLInspector, on obtient alors les temps suivants :
|Signal|Temps minimum|
|:-----:|:----------:|
|Obst_detection| 25ms|
|speed_comput|15ms|
|command_comput|__135ms__|

Ces temps sont corrects du point de vu des temps de latence et de gigue maximum.
On constate de plus dans AADLInspector que ces temps sont un point fixe (lorsque l'on met le modèle avec ces temps, on obtient les mêmes temps de pire réponse).
On a donc déterminé des paramètres corrects.

On cherche ensuite à calculer le meilleur temps selon lequel chaque tâche peut être effectuée. Intuitivement une tâche ne peut être effectuée avec un temps d'exécution inférieur à son meilleur temps de réponse par cycle, ce qui donne comme valeurs initiales :

|Signal|Temps minimum|
|:-----:|:----------:|
|Obst_detection|5ms|
|speed_comput|10ms|
|command_comput|60ms|
