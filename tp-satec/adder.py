#!/usr/bin/env python3

import circuit.circuit as circ
from circuit.cnf import SatVar, Solver, Cnf

# circ full_adder {
#      inputs: a, b, cin
#      outputs: s, cout
#      s0 = a ^ b
#      s = s0 ^ cin
#      cout = (a & b) | (s0 & cin)
# }

# Inputs
a = SatVar('a')
b = SatVar('b')
cin = SatVar('cin')

# Outputs
s = SatVar('s')
cout = SatVar('cout')

# Internal variables (if needed)
s0 = SatVar('s0')
i0 = SatVar('i0')
i1 = SatVar('i1')

def mk_or(x,y,z) -> Cnf:
    return (x | y | ~z) & (~x | z) & (~y | z)

def mk_and(x,y,z) -> Cnf:
    return (~x | ~y | z) & (x | ~z) & (y | ~z)

def mk_not(x,z) -> Cnf:
    return (~x | ~z) & (x|z)

def mk_xor(x,y,z) -> Cnf:
    return ( ~x | ~y | ~z) & (x | y | ~z) & (x | ~y | z) & (~x | y | z)


def mk_adder() -> Cnf:
    return mk_xor(a,b,s0) & mk_and(a,b,i1) & mk_and(cin, s0,i0) & mk_xor(cin,s0,s) & mk_or(i0,i1,cout)


