#!/usr/bin/env python3

from typing import List, Dict

import circuit.circuit as circ
from circuit.cnf import SatVar, Solver, Cnf
from circuit.circuit import Circuit

# Implementation hints:
#
# 1) For all inputs, outputs, and named signals of the circuit, it is
#    required that the variable names in the CNF are the same as the
#    signal names, i.e. after solving the CNF, it must be possible to
#    obtain the assignment of a circuit signal by indexing the
#    solution with the signal name: a = solution['a']. The variable
#    names for any other internal nodes can be chosen freely.  If a
#    prefix is given, then all variable names must begin with this
#    prefix.
#
# 2) In order to implement the transformation, you will need to
#    traverse the circuit graph for all outputs (and named internal
#    signals). Make sure that you do not forget any of the defined
#    node types. Check the file circuit.py to find out about the
#    possible node types. You can use the function node.getID() in
#    order to obtain a unique identifier for a node.
#
# 3) Test your code! There is a test script named test.py. If your
#    code passes all the tests, there is a good chance that it is
#    correct.

# Primitives functions for the Tseiting transformation


def mk_or(x, y, z) -> Cnf:
    return (x | y | ~z) & (~x | z) & (~y | z)


def mk_and(x, y, z) -> Cnf:
    return (~x | ~y | z) & (x | ~z) & (y | ~z)


def mk_not(x, z) -> Cnf:
    return (~x | ~z) & (x | z)


def mk_xor(x, y, z) -> Cnf:
    return (~x | ~y | ~z) & (x | y | ~z) & (x | ~y | z) & (~x | y | z)


def mk_is(x,z) -> Cnf:
    return (~x | z) & (x | ~z)


# Current transformation function
def transform(c: Circuit, prefix: str = '') -> Cnf:
    '''
    The function transform takes a Circuit c and returns a Cnf obtained by the
    Tseitin transformation of c. The optional prefix string will be used for
    all variable names in the Cnf.
    '''

    # dictionary to get the SatVars
    satvars_map = {}

    # create the new SatVar if needed or get existing ones with the prefix
    def getsatvar(id):
        if id not in satvars_map:
            satvars_map[id] = SatVar(prefix + str(id))
        return satvars_map[id]

    # Internal function to treat one node
    # if the node has a name, then the name is passed in node_name
    def transform_node(node: circ.Node, node_name: str = "") -> Cnf:
        # Hold the satvars to get as parameters in the operation
        satvar_list: List[SatVar] = []
        # The head node has no name
        if node_name == "":
            satvar_list.append(getsatvar(node.getID()))
        else:
            # The node has a name and it is representing a variable (directly connected variable)
            if type(node) == circ.Variable:
                return mk_is(getsatvar(node.getName()), getsatvar(node_name))
            # The node is a literal then the value of the literal should be appened to the CNF with its phase
            elif type(node) == circ.Literal:
                if node.getValue():
                    return getsatvar(node_name)
                else:
                    return ~getsatvar(node_name)
            else:
                # It is a named head of an operation
                satvar_list.append(getsatvar(node_name))
        # We get its children
        children = node.getChildren()
        for child in children:
            # The child node is a variable, so we get it by its name
            if type(child) == circ.Variable:
                satvar_list.append(getsatvar(child.getName()))
            else:
                # The child node is anything else so we get it by its id
                satvar_list.append(getsatvar(child.getID()))
        # If the node has two children then it is a BiOp operation
        # head node is always in satvar_list[0]
        if len(children) == 2:
            if node.getOp() == "|":
                return mk_or(satvar_list[1], satvar_list[2], satvar_list[0])
            elif node.getOp() == "^":
                return mk_xor(satvar_list[1], satvar_list[2], satvar_list[0])
            else:
                return mk_and(satvar_list[1], satvar_list[2], satvar_list[0])
        # The node is a UnOp operation
        elif len(children) == 1:
            return mk_not(satvar_list[1], satvar_list[0])
        else:
            # The node is a literal so we add the literal value to the CNF
            literal: SatVar
            if node.getValue():
                literal = getsatvar(node.getID())
            else:
                literal = ~getsatvar(node.getID())
            return literal

    # Initialisation of all the needed parameters
    nodes_to_treat: List[circ.Node] = []
    nodes_already_seen: List[circ.Node] = []
    answer : Cnf = Cnf()
    # Get all the named node (signals name)
    for signal_name in c.getSignals():
        node = c.getEquation(signal_name)
        # Forward the node to the internal operation to be treated
        answer &= transform_node(node,signal_name)
        # If the node has children then they need to be treated after that
        for child in node.getChildren():
                nodes_to_treat.append(child)
    # all the nodes still in the list of nodes are not named (because they are not the direct result of an equation
    for node_to_treat in nodes_to_treat:
        # check if the node have been seen already
        if node_to_treat not in nodes_already_seen:
            # nothing to do with variables, they provide no CNF
            if type(node_to_treat) != circ.Variable:
                # provide the node to the internal function
                answer &= transform_node(node_to_treat,"")
            # repeat the process of adding the children to the list of nodes to visit
            for child in node_to_treat.getChildren():
                    if child not in nodes_to_treat:
                        nodes_to_treat.append(child)
            # add the node just seen in the list to not go back on this one
            nodes_already_seen.append(node)
    return answer