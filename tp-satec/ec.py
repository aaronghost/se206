#!/usr/bin/env python3

import sys

import circuit.circuit as circ
from circuit.cnf import SatVar, Solver, Solution, Cnf
from circuit.circuit import Circuit
from transform import transform, mk_is, mk_xor


# Implementation hints:
#
# 1) You need to build a *miter circuit* (or rather a miter CNF) in order to
#    compare the two circuits. For the circuit part, you can just use the
#    transform function from Exercise 2 (make sure to use different prefixes
#    for the two circuits, as they will usually have overlapping variable
#    names).
#
# 2) Make sure you cover the following error conditions:
#    * The two circuits have different number of inputs or outputs
#    * The inputs and outputs of the two circuits do not have the same names
#    In these cases you can return (False, None).
#
# 3) Run the test script to see if your code works!

def check(c1: Circuit, c2: Circuit) -> (bool, Solution):
    '''The function check() takes two Circuits as input and performs an equivalence
    check using a SAT solver. it returns a tuple, where the first entry is a
    Boolean value (True for equivalent, False for different) and the second
    value is -- in case of a difference found -- a Solution to the underlying
    SAT problem, representing a counterexample. If the circuits are indeed
    equivalent, the second entry will be None.

    '''
    complete_cnf = Cnf()
    # Transformation of the first circuit
    cnf_c1 = transform(c1,"c1_")
    # Transformation of the second circuit
    cnf_c2 = transform(c2,"c2_")

    complete_cnf &= cnf_c1 & cnf_c2

    # Connect entries (we use the is primitive function from the previous stage
    for entry in c1.getInputs():
        if entry not in c2.getInputs():
            return False, None
        else:
            complete_cnf &= mk_is(SatVar("c1_"+entry),SatVar("c2_"+entry))

    # connecting the output by pair from the both circuits with xor
    output_satvars = []
    for exit in c1.getOutputs():
        if exit not in c2.getOutputs():
            return False, None
        else:
            complete_cnf &= mk_xor(SatVar("c1_"+exit),SatVar("c2_"+exit),SatVar("output_"+exit))
            output_satvars.append(SatVar("output_"+exit))

    # connecting the resulting signals with a single or operation using all the signals in the output, we do not need more signal as we are trying to see the result of this formula
    or_cnf = output_satvars[0]
    if len(output_satvars) > 1:
        for output_satvar in output_satvars[1:]:
            or_cnf |= output_satvar
    complete_cnf &= or_cnf

    # we now possess the complete cnf of the system so we solve it
    solver = Solver()
    solution = solver.solve(complete_cnf)
    # If the solution is SAT, it means that a solution exists such as the result is 1 (True) this is our counter-example
    return not solution, None if not solution else solution